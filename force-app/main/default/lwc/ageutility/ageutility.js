


// method to check leap year
function isLeapYear(year) {
    var d = new Date(year, 1, 28);
    d.setDate(d.getDate() + 1);
    return d.getMonth() == 1;
}

// call this method from any LWC by importing this module if you pass cuurent date as null it will calculate till today else the date mentioned
const getAge = (date , currentDate) =>{
    var d = new Date(date), now = (currentDate)?new Date(currentDate) : new Date() ;
   
    var years = now.getFullYear() - d.getFullYear();
    d.setFullYear(d.getFullYear() + years);
    if (d > now) {
        years--;
        d.setFullYear(d.getFullYear() - 1);
    }
    var days = (now.getTime() - d.getTime()) / (3600 * 24 * 1000);
    return years + days / (isLeapYear(now.getFullYear()) ? 366 : 365);
}



export {
    getAge
};